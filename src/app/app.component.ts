import { Component } from '@angular/core';

import { EventTableModel } from './table/event-table-model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public eventTableModel: EventTableModel) {}
}
