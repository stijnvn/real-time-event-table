import { Observable } from 'rxjs';
import { FluxEvent } from './event';

export class EventRecord {
  id: string;
  events: FluxEvent[];
  total: number;
}
export interface TableModel {
  getEventsAroundID(id: string, before: number, after: number): Observable<EventRecord>;
  getEventsAroundTimestamp(timestamp: string, before: number, after: number): Observable<EventRecord>;
}
