export interface FluxEvent {
  index: string;
  name: string;
}
