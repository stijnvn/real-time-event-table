import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/scrolling';
import { AfterViewInit, Component, ElementRef, Input, NgZone, ViewChild } from '@angular/core';

import { FluxEvent } from './event';
import { EventTableModel } from './event-table-model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements AfterViewInit {
  @Input()
  model: EventTableModel;

  @ViewChild('viewport', { read: CdkScrollable })
  scrollable: CdkScrollable;

  @ViewChild('viewport')
  viewport: ElementRef;

  // FIXME: Read height from the DOM.
  eventHeight = 100;

  get rectangleHeight(): number {
    const maxHeight = 1780000;
    return Math.min(this.totalEventHeight, maxHeight);
  }

  get contentOffset(): number {
    return (this.eventPointer * this.eventHeight) / this.rectangleScaleFactor;
  }

  private readonly unknownID = '-1';

  private eventPointer = 0;

  private firstEventInViewportPointer = 0;

  private totalNumberOfEvents = 0;

  private eventCache = new Map<string, FluxEvent>();

  private viewportEventIDs: Array<{ id: string }> = [];

  private get totalEventHeight(): number {
    return this.eventHeight * this.totalNumberOfEvents;
  }

  private get rectangleScaleFactor(): number {
    return this.totalEventHeight / this.rectangleHeight;
  }

  private get viewportHeight(): number {
    return (this.viewport.nativeElement as HTMLElement).clientHeight;
  }

  private get numberOfEventsInViewport(): number {
    return Math.ceil(this.viewportHeight / this.eventHeight);
  }

  constructor(private scrollDispatcher: ScrollDispatcher, private zone: NgZone) {}

  ngAfterViewInit(): void {
    this.scrollDispatcher.register(this.scrollable);
    this.scrollable.elementScrolled().subscribe(event => this.onScroll(event));

    setTimeout(() => {
      // FIXME: unsubscribe?
      this.model.getEventsAroundTimestamp('id:0', 0, 2 * this.numberOfEventsInViewport - 1).subscribe(record => {
        this.viewportEventIDs = record.events.map(event => ({ id: event.index }));
        this.firstEventInViewportPointer = this.eventPointer;
        record.events.forEach(event => this.eventCache.set(event.index, event));
        this.totalNumberOfEvents = record.total;
      });
    });
  }

  getEvent(viewportIndex: number): FluxEvent {
    const index = this.eventPointer - this.firstEventInViewportPointer + viewportIndex;
    if (index < 0 || index >= this.viewportEventIDs.length) {
      return undefined;
    }
    const eventID = this.viewportEventIDs[index].id;
    return this.eventCache.get(eventID);
  }

  private onScroll(scrollEvent: Event): void {
    this.zone.run(() => {
      const scrollTop = (scrollEvent.target as HTMLElement).scrollTop * this.rectangleScaleFactor;
      // FIXME: pass as param instead of storing as member
      this.eventPointer = (scrollTop - (scrollTop % this.eventHeight)) / this.eventHeight;
      const viewportPointer = this.eventPointer - this.firstEventInViewportPointer;
      if (this.isInViewport(viewportPointer)) {
        this.fetchEventsInViewport(viewportPointer);
      } else {
        this.fetchEventsOutsideViewport();
      }
    });
  }

  private isInViewport(viewportPointer: number): boolean {
    return viewportPointer >= 0 && viewportPointer < this.viewportEventIDs.length;
  }

  private fetchEventsInViewport(viewportPointer: number): void {
    const id = this.viewportEventIDs[viewportPointer].id;
    const oldIndex = this.eventPointer;
    if (id !== this.unknownID) {
      this.model.getEventsAroundID(id, 0, 2 * this.numberOfEventsInViewport - 1).subscribe(record => {
        const diff = this.eventPointer - oldIndex;
        for (let i = 0; i < this.viewportEventIDs.length - diff; i++) {
          this.viewportEventIDs[i].id = record.events[i + diff].index;
        }
        for (let i = this.viewportEventIDs.length - diff; i < this.viewportEventIDs.length && i >= 0; i++) {
          this.viewportEventIDs[i].id = this.unknownID;
        }
        this.firstEventInViewportPointer = this.eventPointer;
        record.events.forEach(event => this.eventCache.set(event.index, event));
        this.totalNumberOfEvents = record.total;
      });
    } else {
      this.fetchEventsOutsideViewport();
    }
  }

  private fetchEventsOutsideViewport(): void {
    this.eventCache.clear();
    this.viewportEventIDs.forEach(item => (item.id = this.unknownID));
    // TODO: get timestamp instead of ID.
    const id = `id:${this.eventPointer}`;
    const oldIndex = this.eventPointer;
    this.model.getEventsAroundID(id, 0, 2 * this.numberOfEventsInViewport - 1).subscribe(record => {
      const diff = this.eventPointer - oldIndex;
      for (let i = 0; i < this.viewportEventIDs.length - diff; i++) {
        this.viewportEventIDs[i].id = record.events[i + diff].index;
      }
      for (let i = this.viewportEventIDs.length - diff; i < this.viewportEventIDs.length && i >= 0; i++) {
        this.viewportEventIDs[i].id = '-1';
      }
      this.firstEventInViewportPointer = this.eventPointer;
      record.events.forEach(event => this.eventCache.set(event.index, event));
      this.totalNumberOfEvents = record.total;
    });
  }
}
