import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { TableModel, EventRecord } from './table-model';
import { FluxEvent } from './event';

@Injectable({
  providedIn: 'root'
})
export class EventTableModel implements TableModel {
  private events: FluxEvent[] = [];

  constructor() {
    this.events = [...Array(10000).keys()].map(index => ({ index: `id:${index}`, name: `event ${index}` }));
  }

  getEventsAroundID(id: string, before: number, after: number): Observable<EventRecord> {
    let index = parseInt(id.substr(3), 10) - before;
    if (index < 0) {
      before += index;
      index = 0;
    }
    const amount = before + after + 1;
    const events = this.events.slice(index, index + amount);

    return of({
      id: this.events[index + before].index,
      events,
      total: this.events.length
    }).pipe(delay(2000));
  }

  getEventsAroundTimestamp(timestamp: string, before: number, after: number): Observable<EventRecord> {
    let index = parseInt(timestamp.substr(3), 10) - before;
    if (index < 0) {
      before += index;
      index = 0;
    }
    const amount = before + after + 1;
    const events = this.events.slice(index, index + amount);

    return of({
      id: this.events[index + before].index,
      events,
      total: this.events.length
    }).pipe(delay(2000));
  }
}
